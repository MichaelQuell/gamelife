Author: Michael Quell <michael.quell@tuwien.ac.at> Student Id:01226394

Program name: GameOfLife

Copyright:  You are allowed to copy, modify, compile , distribute the file or do whatever you want unless you remove this copyright notice.

----
#Run
	gameOfLife  gui   [ --alive=<char> --dead=<char> --color --stages=<number>] [ ( --xdim=<number> --ydim=<number> --percent=<number> | --load=<file> ) ] 
	gameOfLife <file> [ --nsteps=<number> ] [ ( --xdim=<number> --ydim=<number> --percent=<number> | --load=<file> ) ]
There are 2 modes to run the program,

* GUI/Interactive

* Command line/Iterate.

----
##Mode GUI

keyword: "gui"

**Options**

**--alive** sets the displayed character for a living cell overwritten by --color

**--dead** sets the displayed character for a dead cell

**--color** enables advanced display of information for alive cells

**--stages** sets the number of stages stored, determines how many steps you can revert

**--xdim --ydim** sets the size of the plane for the game if none is specified the size of the terminal window will be chosen

**--percent** how many % of the cells should be alive in the beginning if not specified 0 is used

**--load** loads a pattern stored in <file>


In the GUI top line shows some statistics.
Beyond that you see the field where the game is played. If the field is bigger then for the terminal possible to display it is cut of on the right and the lower border.

You can interact by pressing Keys:

**Arrow keys** move the courser.

**Space** changes the state of the cell at the current position

**f** computes the next step

**b** goes on step back if possible

**s** starts saving progress, abort by supplying an empty string

**l** starts loading progress, abort by supplying an empty string

**c** toggles the --color option

**q** quits the program

----
##Mode Command line
keyword: a <file> not named "gui" 

**<file>** there will be the output stored as text file

**--nsteps** sets the number of steps in the game of life to be computed

**--xdim --ydim --percentage --load** as specified in Mode GUI

----
#Compile
Run the makefile. You also need a ncurses installed.
Tested on Ubuntu16.04 and in Cygwin.
