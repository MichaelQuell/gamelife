#
# Author: Michael Quell <michael.quell@tuwien.ac.at> Student Id:01226394
# Programname: GameOfLife
# \copyright { You are allowed to copy, modify, compile , distribute the file or do whatever you want unless you remove this copyright notice.}
#

CFLAGS=-std=c99 -pedantic -Wall -O3 -D_XOPEN_SOURCE=500 -D_DEFAULT_SOURCE -g -c

.PHONY: all clean test1 test2 test3

all:
		@echo "First try to build with gui, if failed try without gui"
		$(MAKE) gui || ($(MAKE) error; $(MAKE) nogui)
gui: src/main.c
		gcc -DGUI $(CFLAGS) ./src/main.c
		gcc  -o gameOfLife  main.o -lncurses
		@echo "BUILT WITH GUI SUCCESS"
error:
		@echo "Trying to build without gui"
nogui: src/main.c
		gcc $(CFLAGS) ./src/main.c
		gcc  -o gameOfLife  main.o
		@echo "BUILT WITHOUT GUI SUCCESS"
test1:
		./gameOfLife image.bmp -x 1080 -y 1920 -p 40 -n 100
test2:
		./gameOfLife gui -l data
test3:
		./gameOfLife gui -p 44 -c -s 100
clean:
		rm -f gameOfLife
		rm -f main.o

