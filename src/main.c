/*
# Author: Michael Quell <michael.quell@tuwien.ac.at> Student Id:01226394
# Programname: GameOfLife
# \copyright { You are allowed to copy, modify, compile , distribute the file or do whatever you want unless you remove this copyright notice.}
*/
#include<unistd.h>
#include<assert.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdarg.h>
#include<errno.h>
#include<limits.h>
#include<malloc.h>
#include<getopt.h>

#ifdef GUI
#include<ncurses.h>
#endif

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define BUFFER 1000


enum{dead=0, alive};

enum{off=0, on};

struct options{
	int x;
	size_t x_val;  //size in x-dimension
	int y;
	size_t y_val; //size in y-dimension
	int n;
	size_t n_val; //how opten to iterate
	int c; //enable color output
	int s;
	size_t s_val; //how many previous steps to be stored
	int l;
	char* l_val; //filename to be loaded
	int p;
	size_t p_val; //percentage of alive during initialisation
	int a;
	char c_alive; //symbol for alive cells
	int d;
	char c_dead; //symbol for dead cells
	int gui; //enabales gui
};

/*FUNCTIONS*/

/*If something goes wrong.*/
static void bail_out(int exitcode, const char *fmt, ...);
/*Parse the input*/
static void parse_args(int argc, char **argv);
/*Helps parse_args*/
static void parse_opt(char* c, int* n, size_t* n_val);
/*free all allocated resourses*/
static void free_resources(void);
#ifdef GUI
/*main function for the gui*/
static void gui(void);
/*draws the screen when gui is enabled*/
static void wdraw(void);
/*draws the headline when gui is enabled*/
static void drawhead(int ch);
#endif
/*main function without the gui*/
static void nogui(char* filename);
/*allocates the array (dataArray) for the cells*/
static void mallocDataArray(void);
/*frees the array (dataArray) of the cells*/
static void freeDataArray(void);
/*sets array (dataArray) to dead and creates initial alive population*/
static void initialise(void);
#ifdef DEBUG
/*prints current state to stdout*/
static void draw(void);
#endif
/*computes a step in the game of life*/
static void update(void);
/*returns the number of alive neigbours*/
static unsigned int getNeighbours(size_t i, size_t j);
/*saves the current state to disk*/
static int save(const char* filename);
/*saves the current state as bmp to disk*/
static int saveimg(const char* filename);
/*loads a state from disk*/
static int load(const char* filename);
/*prints usage message*/
static void usage(int ret);

/*Global variables*/
char*progname="GameOfLife";
struct options opt;
size_t cDataArray=0;
size_t bDataArray=0;
char *** dataArray=NULL;

/*main function*/
int main(int argc, char* argv[]){
	parse_args(argc, argv);
	if(opt.gui==on){
#ifdef GUI
		gui();
#else
		bail_out(EXIT_FAILURE,"Built without GUI");
#endif
	}else{
		nogui(argv[optind]);
	}
	free_resources();
	return EXIT_SUCCESS;
}

/*FUNCTION BODY*/

static void nogui(char* filename){
	if(opt.l==on){
		if(load(opt.l_val)!=-1){
			bail_out(EXIT_FAILURE,"Error reading file %s: ",opt.l_val);
		}
	}else{
		mallocDataArray();
		initialise();
	}
	size_t i;
	for(i=0;i<opt.n_val;i++){
		update();
	}
	int ret=0;
	if(strlen(filename)>4&&strncmp(&(filename[strlen(filename)-4]),".bmp",4)==0){
		ret=saveimg(filename);
	}else{
		ret=save(filename);
	}
	if(ret!=0){
		bail_out(EXIT_FAILURE,"Error writing file %s: ",filename);
	}
}

#ifdef GUI
static void gui(void){
	initscr();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	start_color();
	use_default_colors();
	init_pair(1,COLOR_WHITE  ,COLOR_BLACK);
	init_pair(2,COLOR_RED    ,COLOR_BLACK);
	init_pair(3,COLOR_GREEN  ,COLOR_BLACK);
	init_pair(4,COLOR_YELLOW ,COLOR_BLACK);
	init_pair(5,COLOR_BLUE   ,COLOR_BLACK);
	init_pair(6,COLOR_MAGENTA,COLOR_BLACK);
	bkgd(COLOR_PAIR(1));
	
	int ret=0;
	ret=load(opt.l_val);
	if(ret>=0){
		if(opt.l==on){
			mvprintw(0,0,"Error reading file, line: %d",ret);
			if (errno != 0) {
				printw(": %s", strerror(errno));
			}
			refresh();
			sleep(1);
		}
		if(opt.x==off&&opt.y==off){
			getmaxyx(stdscr,opt.x_val,opt.y_val);
			opt.x_val=opt.x_val-1;
		}
		mallocDataArray();
		initialise();
	}else{
		mvprintw(0,0,"File loaded: %s",opt.l_val);
		clrtoeol();
		refresh();
		sleep(1);
	}
	int curserX=0;
	int curserY=0;
	clear();
	newwin(0,0,opt.x_val,opt.y_val);
	wdraw();
	move(curserX+1,curserY);
	refresh();
	int ch;
	char filename[BUFFER];
	while((ch = getch()) != 'q') /*quit the program*/
	{	switch(ch)
		{	case KEY_LEFT: /*move curser to the left*/
				curserY=(curserY-1+opt.y_val)%opt.y_val;
				break;
			case KEY_RIGHT: /*move curser to the right*/
				curserY=(curserY+1)%opt.y_val;
				break;
			case KEY_UP: /*move curser up*/
				curserX=(curserX-1+opt.x_val)%opt.x_val;
				break;
			case KEY_DOWN: /*move curser down*/
				curserX=(curserX+1)%opt.x_val;
				break;
			case KEY_RESIZE:
				wdraw();
				break;
			case 'f': /*compute 1 step in the game of life*/
				update();
				bDataArray=MIN(bDataArray+1,opt.s_val);
				opt.n_val++;
				wdraw();
				break;
			case 'b': /*load previous step in the game of life*/
				if(bDataArray>0){
					cDataArray=(cDataArray-1+opt.s_val)%opt.s_val;
					wdraw();
					bDataArray--;
					opt.n_val--;
				}
				break;
			case 'c': /*enabales/disables color*/
				opt.c=!opt.c;
				wdraw();
				break;
			case ' ': /*change the current element*/
				if(dataArray[cDataArray][curserX][curserY]==alive){
					dataArray[cDataArray][curserX][curserY]=dead;
					mvaddch(curserX+1,curserY,opt.c_dead);
				}else{
					dataArray[cDataArray][curserX][curserY]=alive;
					mvaddch(curserX+1,curserY,opt.c_alive);
				}
				break;
			case 's': /*save the game*/
				echo();
				mvprintw(0,0,"Save to: ");
				clrtoeol();
				getnstr(filename,BUFFER-1);
				if(strlen(filename)==0){
					wdraw();
					break;
				}
				if(strlen(filename)>4&&strncmp(&(filename[strlen(filename)-4]),".bmp",4)==0){
					ret=saveimg(filename);
				}else{
					ret=save(filename);
				}
				if(ret!=0){
					mvprintw(0,0,"Error writing file: ");
					if (errno != 0) {
						printw(": %s", strerror(errno));
					}
				}else{
				mvprintw(0,0,"File written: %s",filename);
				}
				clrtoeol();
				refresh();
				sleep(1);
				wdraw();
				noecho();
				break;
			case 'l': /*load the game*/
				echo();
				mvprintw(0,0,"Load from: ");
				clrtoeol();
				getnstr(filename,BUFFER-1);
				if(strlen(filename)==0){
					wdraw();
					break;
				}
				ret=load(filename);
				if(ret>=0){
					mvprintw(0,0,"Error reading file, line: %d",ret);
					if (errno != 0) {
						printw(": %s", strerror(errno));
					}
				}else{
				mvprintw(0,0,"File loaded: %s",filename);
				clrtoeol();
				}
				refresh();
				sleep(1);
				clear();
				newwin(0,0,opt.x_val,opt.y_val);
				wdraw();
				noecho();
				break;
			default:
				drawhead(ch);
				break;
		}
	move(curserX+1,curserY);
	refresh();
	}
	endwin();
}

static void drawhead(int ch){
	attron(A_STANDOUT); mvprintw(0,0,"%c",(char)ch); attroff(A_STANDOUT);
	addstr(":invalid key ");
	attron(A_STANDOUT); addstr("<^>v"); attroff(A_STANDOUT);
	addstr(":move curser ");
	attron(A_STANDOUT); addstr(" "); attroff(A_STANDOUT);
	addstr(":change ");
	attron(A_STANDOUT); addstr("f"); attroff(A_STANDOUT);
	addstr(":foward ");
	attron(A_STANDOUT); addstr("b"); attroff(A_STANDOUT);
	addstr(":backward ");
	attron(A_STANDOUT); addstr("s"); attroff(A_STANDOUT);
	addstr(":save ");
	attron(A_STANDOUT); addstr("l"); attroff(A_STANDOUT);
	addstr(":load ");
	attron(A_STANDOUT); addstr("c"); attroff(A_STANDOUT);
	addstr(":color ");
	attron(A_STANDOUT); addstr("q"); attroff(A_STANDOUT);
	addstr(":quit");
	clrtoeol();
}
static void wdraw(void){
	size_t i,j;
	size_t nalive=0;
	size_t oldest=0;
	size_t born=0;
	size_t died=0;
	for(i=0;i<opt.x_val;i++){
		for(j=0;j<opt.y_val;j++){
			if(dataArray[cDataArray][i][j]==alive){
				nalive++;
				if(dataArray[(cDataArray-1+opt.s_val)%opt.s_val][i][j]==dead){
					born++;
				}
				size_t cAlive=0;
				while(dataArray[(cDataArray-cAlive-1+opt.s_val)%opt.s_val][i][j]==alive&&cAlive<bDataArray){
					cAlive++;
				}
				oldest=MAX(oldest,cAlive);
				if(opt.c==on){
					if(cAlive<60){
						attrset(COLOR_PAIR(MIN(cAlive/10+1,6)));
						mvaddch(i+1,j,(char)(cAlive%10+'0'));
						attrset(COLOR_PAIR(1));
					}else{
						cAlive=MIN(cAlive-59,59);
						attrset(COLOR_PAIR(MIN(cAlive/10+1,6)));
						attron(A_STANDOUT);
						mvaddch(i+1,j,(char)(cAlive%10+'0'));
						attrset(COLOR_PAIR(1));
						attroff(A_STANDOUT);
					}
				}else{
					mvaddch(i+1,j,opt.c_alive);
				}
			}else{
				if(dataArray[(cDataArray-1+opt.s_val)%opt.s_val][i][j]==alive){
					died++;
				}
				mvaddch(i+1,j,opt.c_dead);
			}
		}
	}
	mvprintw(0,0,"Alive:%zu Oldest:%zu Born:%zu Died:%zu Step:%zu Back:%zu ",nalive,oldest,born,died,opt.n_val,bDataArray);
	clrtoeol();
}
#endif

static void bail_out(int exitcode, const char *fmt, ...){
#ifdef GUI
	if(opt.gui==on){
		clear();
		endwin();
	}
#endif
	va_list ap;
	(void) fprintf(stderr, "%s: ", progname);
	if (fmt != NULL) {
		va_start(ap, fmt);
		(void) vfprintf(stderr, fmt, ap);
		va_end(ap);
	}
	if (errno != 0) {
		(void) fprintf(stderr, ": %s", strerror(errno));
	}
	(void) fprintf(stderr, "\n");
	free_resources();
	exit(exitcode);
}


static void free_resources(void){
	freeDataArray();
}

static void parse_args(int argc, char **argv){
	if(argc > 0) {
		progname = argv[0];
	}
	int c=0;
	memset(&opt,0,sizeof(struct options));
	opt.c_alive='#';
	opt.c_dead='.';
	
	static struct option long_options[] = {
		{"xdim",    required_argument, 0,  'x'},
		{"ydim",    required_argument, 0,  'y' },
		{"nsteps",  required_argument, 0,  'n' },
		{"color",   no_argument,       0,  'c' },
		{"stages",  required_argument, 0,  's' },
		{"load",    required_argument, 0,  'l' },
		{"percent", required_argument, 0,  'p' },
		{"alive",   required_argument, 0,  'a' },
		{"dead",    required_argument, 0,  'd' },
		{"help",    no_argument,       0,  'h' },
		{0,         0,                 0,   0 }
	};
	int option_index=0;
	while(c!=-1){
		option_index=0;
		c=(int) getopt_long(argc, argv,"x:y:n:cs:l:p:a:d:h",long_options,&option_index);
		switch(c){
			case 'x':
				parse_opt("--xdim", &opt.x, &opt.x_val);
				break;
			case 'y':
				parse_opt("--ydim", &opt.y, &opt.y_val);
				break;
			case 'n':
				parse_opt("--nsteps", &opt.n, &opt.n_val);
				break;
			case 'c':
				opt.c++;
				break;
			case 's':
				parse_opt("--stages", &opt.s, &opt.s_val);
				break;
			case 'l':
				opt.l++;
				opt.l_val=optarg;
				break;
			case 'p':
				parse_opt("--percent", &opt.p, &opt.p_val);
				if(opt.p_val>100){
					bail_out(EXIT_FAILURE,"0<=--percent<=100 required");
					}
				break;
			case 'a':
				opt.a++;
				if(strlen(optarg)>1){
					bail_out(EXIT_FAILURE,"Argument \" %s \" for --alive too long",optarg);
				}
				opt.c_alive=optarg[0];
				break;
			case 'd':
				opt.d++;
				if(strlen(optarg)>1){
					bail_out(EXIT_FAILURE,"Argument \" %s \" for --dead too long",optarg);
				}
				opt.c_dead=optarg[0];
				break;
			case 'h':
				usage(EXIT_SUCCESS);
				break;
			case '?': /*wrong option*/
				usage(EXIT_FAILURE);
				break;
			case -1: /*no more options*/
				break;
			default:
				assert(0);/*this is impossible*/
		}
	}
	opt.x_val=MAX(opt.x_val,1);
	opt.y_val=MAX(opt.y_val,1);
	opt.s_val=MAX(opt.s_val,2);
	if (opt.x>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--xdim more than once");
	}
	if (opt.y>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--ydim more than once");
	}
	if (opt.n>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--nsteps more than once");
	}
	if (opt.c>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--color more than once");
	}
	if (opt.s>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--stages more than once");
	}
	if (opt.l>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--load more than once");
	}
	if (opt.p>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--percent more than once");
	}
	if (opt.a>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--alive more than once");
	}
	if (opt.d>1){ /*option only once*/
		bail_out(EXIT_FAILURE,"--dead more than once");
	}
	if (argc-optind!=1){
		usage(EXIT_FAILURE);
	}else{
		if(strlen(argv[optind])>=3&&strncmp(argv[optind],"gui",3)==0){
			opt.gui=on;
			if(opt.n>0){
				usage(EXIT_FAILURE);
			}
		}else{
			if(opt.a>0||opt.d>0||opt.c>0||opt.s>0){
				usage(EXIT_FAILURE);
			}
		}
	}
	if(opt.l>0&&(opt.x>0||opt.y>0||opt.p>0)){
		usage(EXIT_FAILURE);
	}
}

void parse_opt(char* c,int* opt, size_t* opt_val){
	char* endptr;
	(*opt)++;
	errno=0;
	*opt_val=(int) strtoull(optarg,&endptr,10);
	//fprintf(stderr, "%s#%zu\n%s\n",optarg,x,strerror(errno));
	if(optarg[0]=='-'||*endptr!='\0'||errno==ERANGE){
		bail_out(EXIT_FAILURE,"Invalid argument for %s",c);
	}
	if(c[3]!='p'&&c[3]!='n'&&*opt_val==0){
		bail_out(EXIT_FAILURE,"0 is an invalid argument for %s",c);
	}
}

static void mallocDataArray(void){
	size_t i,j;
	dataArray=(char***)malloc(opt.s_val*sizeof(char**));
	if(dataArray==NULL){
		bail_out(EXIT_FAILURE,"Could not allocate array");
	}
	for(i=0;i<opt.s_val;i++){
		dataArray[i]=(char**)malloc(opt.x_val*sizeof(char*));
		if(dataArray[i]==NULL){
			bail_out(EXIT_FAILURE,"Could not allocate array");
		}
		for(j=0;j<opt.x_val;j++){
			dataArray[i][j]=(char*)malloc(opt.y_val*sizeof(char));
			if(dataArray[i][j]==NULL){
				bail_out(EXIT_FAILURE,"Could not allocate array");
			}
		}
	}
}

static void freeDataArray(void){
	if(dataArray!=NULL){
		size_t i,j;
		for(i=0;i<opt.s_val;i++){
			if(dataArray[i]!=NULL){
				for(j=0;j<opt.x_val;j++){
					if(dataArray[i][j]!=NULL){
						free(dataArray[i][j]);
						dataArray[i][j]=NULL;
					}
				}
				free(dataArray[i]);
				dataArray[i]=NULL;
			}
		}
		free(dataArray);
		dataArray=NULL;
	}
}

static void initialise(void){
	srand(0);
	size_t i,j,k;
	for(i=0;i<opt.s_val;i++){
		for(j=0;j<opt.x_val;j++){
			memset((void*)dataArray[i][j],dead,opt.y_val*sizeof(char));
			if(opt.p==on&&i==cDataArray){
				for(k=0;k<opt.y_val;k++){
					if(rand()%100<opt.p_val){
						dataArray[i][j][k]=alive;
					}
				}
			}
		}
	}
}

#ifdef DEBUG
static void draw(void){
	size_t i,j;
	for(i=0;i<opt.x_val;i++){
		for(j=0;j<opt.y_val;j++){
			if(dataArray[cDataArray][i][j]==alive){
				fprintf(stdout,"%c",opt.c_alive);
			}else{
				fprintf(stdout,"%c",opt.c_dead);
			}
		}
		fprintf(stdout,"\n");
	}
}
#endif


static void update(void){
	size_t i,j;
	unsigned int neigbours;
	for(i=0;i<opt.x_val;i++){
		memset((void*)dataArray[(cDataArray+1)%opt.s_val][i],dead,opt.y_val*sizeof(char));
		for(j=0;j<opt.y_val;j++){
			neigbours=getNeighbours(i,j);
			if(dataArray[cDataArray][i][j]==dead && neigbours==3){
				dataArray[(cDataArray+1)%opt.s_val][i][j]=alive;
				continue;
			}
			if(dataArray[cDataArray][i][j]==alive && (neigbours==2 || neigbours==3)){
				dataArray[(cDataArray+1)%opt.s_val][i][j]=alive;
			}
		}
	}
	cDataArray=(cDataArray+1)%opt.s_val;
}

static unsigned int getNeighbours(size_t i, size_t j){
	unsigned int ret=0;
	ret+=dataArray[cDataArray][((i-1+opt.x_val)%opt.x_val)][(j-1+opt.y_val)%opt.y_val];
	ret+=dataArray[cDataArray][((i-1+opt.x_val)%opt.x_val)][ j                       ];
	ret+=dataArray[cDataArray][((i-1+opt.x_val)%opt.x_val)][(j+1          )%opt.y_val];
	ret+=dataArray[cDataArray][  i                        ][(j-1+opt.y_val)%opt.y_val];
	ret+=dataArray[cDataArray][  i                        ][(j+1          )%opt.y_val];
	ret+=dataArray[cDataArray][((i+1          )%opt.x_val)][(j-1+opt.y_val)%opt.y_val];
	ret+=dataArray[cDataArray][((i+1          )%opt.x_val)][ j                       ];
	ret+=dataArray[cDataArray][((i+1          )%opt.x_val)][(j+1          )%opt.y_val];
	return ret;
}

int save(const char* filename){
	FILE* file=NULL;
	file=fopen(filename,"w");
	if(file==NULL){
		return errno;
	}
	size_t i,j;
	for(i=0;i<opt.x_val;i++){
		for(j=0;j<opt.y_val;j++){
			fprintf(file,"%d",(int)dataArray[cDataArray][i][j]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
	file=NULL;
	return 0;
}

int saveimg(const char* filename){
	size_t i,j;
	size_t header=54;
	unsigned char*picture=(unsigned char*)malloc((3*opt.x_val*opt.y_val+header)*sizeof(unsigned char));
	if(picture==NULL){
		return errno;
	}
	for(i=0;i<opt.x_val;i++){
		for(j=0;j<opt.y_val;j++){
			if(dataArray[cDataArray][i][j]==alive){
				picture[3*(i*opt.y_val+j)+header+0]=(unsigned char)(255);
				picture[3*(i*opt.y_val+j)+header+1]=(unsigned char)(255);
				picture[3*(i*opt.y_val+j)+header+2]=(unsigned char)(255);
			}
			else{
				picture[3*(i*opt.y_val+j)+header+0]=(unsigned char)(0);
				picture[3*(i*opt.y_val+j)+header+1]=(unsigned char)(0);
				picture[3*(i*opt.y_val+j)+header+2]=(unsigned char)(0);
			}
		}
	}
	//header for bmp file
	int padSize=(4-opt.y_val%4)%4;
	size_t filesize=header + 3*opt.x_val*opt.y_val+opt.x_val*padSize;
	unsigned char bmppad[3] = {0,0,0}; //padding
	picture[ 0]='B';
	picture[ 1]='M';
	picture[ 2] = (unsigned char)(filesize    );
	picture[ 3] = (unsigned char)(filesize>> 8);
	picture[ 4] = (unsigned char)(filesize>>16);
	picture[ 5] = (unsigned char)(filesize>>24);
	picture[ 6] = 0;
	picture[ 7] = 0;
	picture[ 8] = 0;
	picture[ 9] = 0;
	picture[10] = 54;
	picture[11] = 0;
	picture[12] = 0;
	picture[13] = 0;
	picture[14] = 40;
	picture[15] = 0;
	picture[16] = 0;
	picture[17] = 0;//3
	picture[18] = (unsigned char)(       opt.y_val    );
	picture[19] = (unsigned char)(       opt.y_val>> 8);
	picture[20] = (unsigned char)(       opt.y_val>>16);
	picture[21] = (unsigned char)(       opt.y_val>>24);
	picture[22] = (unsigned char)(       opt.x_val    );
	picture[23] = (unsigned char)(       opt.x_val>> 8);
	picture[24] = (unsigned char)(       opt.x_val>>16);
	picture[25] = (unsigned char)(       opt.x_val>>24);
	picture[26] = 1;
	picture[27] = 0;
	picture[28] = 24;
	picture[29] = 0;
	for(i=30;i<54;i++){
		picture[i]=0;
	}
	FILE*file=NULL;
	file=fopen(filename,"wb");
	if (file==NULL) {
		return errno;
	}
	for(i=0;i<header;i++){
		fwrite(&picture[i], sizeof(unsigned char), 1, file);
	}
	for(i=0;i<opt.x_val;i++){
		fwrite(picture+(opt.y_val*(opt.x_val-i-1)*3)+header,
				3* sizeof(unsigned char), opt.y_val, file);
		fwrite(bmppad,sizeof(unsigned char),(4-(opt.y_val*3)%4)%4,file);
	}
	fclose(file);
	free(picture);
	return 0;
}

int load(const char* filename){
	FILE* file=NULL;
	file=fopen(filename,"r");
	if(file==NULL){
		return errno;
	}

	size_t i,j,k;
	char* line=NULL;
	size_t lines=0;
	size_t read;
	size_t len=0;
	size_t tmpy=1;
	while((read=getline(&line,&len,file))!=-1){
		if (lines>1&&read!=tmpy){
			return lines+1; /*not rectengular data*/
		}
		tmpy=read;
		lines++;
	}
	rewind(file);
	freeDataArray();//free
	opt.y_val=tmpy-1;
	opt.x_val=lines;
	mallocDataArray();//allocate
	//load data
	cDataArray=0;
	for(j=0;j<opt.x_val;j++){
		if (getline(&line,&len,file)==opt.y_val+1){
			for(k=0;k<opt.y_val;k++){
				dataArray[cDataArray][j][k]=line[k]-'0';
			}
		}else{
			bail_out(EXIT_FAILURE,"Reading data failed");
		}
	}
	for(i=1;i<opt.s_val;i++){
		for(j=0;j<opt.x_val;j++){
			memset(dataArray[i][j],dead,opt.y_val*sizeof(char));
		}
	}
	free(line);
	fclose(file);
	file=NULL;
	return -1;
}


static void usage(int ret){
	(void) fprintf(stderr,"Usage: %s  gui   [ --alive=<char> --dead=<char> --color --stages=<number>] [ ( --xdim=<number> --ydim=<number> --percent=<number> | --load=<file> ) ] \n       %s <file> [ --nsteps=<number> ] [ ( --xdim=<number> --ydim=<number> --percent=<number> | --load=<file> ) ]\n",progname,progname);
	
	exit(ret);
}
